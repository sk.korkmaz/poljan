﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.Update
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {


                var versionFromParameter = args[0];
                var driverUpdate = args[1];
                var basePath = AppDomain.CurrentDomain.BaseDirectory;
                var fPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "update-" + versionFromParameter + ".zip");
                var extractintPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "update-" + versionFromParameter);
                Directory.CreateDirectory(extractintPath);
                Console.WriteLine(versionFromParameter);
                var res = File.Exists(fPath);
                Console.WriteLine("Dosyalar Çıkartılıyor...");
                if (res)
                {
                    if (Directory.Exists(extractintPath))
                    {
                        Directory.Delete(extractintPath, true);
                    }
                    ZipFile.ExtractToDirectory(fPath, extractintPath);
                }
                Console.WriteLine("İşlemler Sonlandırılıyor...");

                Process.Start(new ProcessStartInfo
                {
                    FileName = "cmd.exe",
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    Arguments = string.Format("/c taskkill /f /IM chromedriver.exe")
                });
                Console.WriteLine("chromedriver sonlandırıldı.");
                Process.Start(new ProcessStartInfo
                {
                    FileName = "cmd.exe",
                    CreateNoWindow = true,
                    UseShellExecute = false,
                    Arguments = string.Format("/c taskkill /f /IM webdri.exe")
                });
                Console.WriteLine("webdri.exe sonlandırıldı.");
                Console.WriteLine("eski dosyalar temizleniyor...");
                List<string> fileList = new List<string>();
                fileList.Add("chromedriver.exe");
                fileList.Add("Microsoft.Edge.SeleniumTools.dll");
                fileList.Add("PolJan.KbsService.dll");
                fileList.Add("PolJan.Models.dll");
                fileList.Add("PolJan.WebDriver.dll");
                fileList.Add("webdri.exe");
                fileList.Add("webdri.exe.config");
                fileList.Add("WebDriver.dll");
                fileList.Add("WebDriver.Support.dll");
                foreach (var item in fileList)
                {
                    Console.WriteLine(item + " siliniyor...");
                    File.Delete(Path.Combine(basePath, item));
                }
                Console.WriteLine("yeni dosyalar kopyalanıyor...");
                var filePaths = new DirectoryInfo(extractintPath).GetFiles();
                for (int i = 0; i < filePaths.Length; i++)
                {
                    File.Copy(Path.Combine(extractintPath, filePaths[i].Name), Path.Combine(basePath, filePaths[i].Name), true);
                }
                //string[] filePaths = Directory.GetFiles(extractintPath);
                //foreach (var item in filePaths)
                //{
                //    File.Copy(extractintPath, basePath);
                //}
                Console.WriteLine("dosyalar kopyalandı");
                Console.WriteLine("Güncelleme Tamamlandı. Lütfen Bildirimi Tekrar Başlatınız. Devam Etmek İçin Bir Tuşa Basınız");
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.ReadLine();
            }

        }
    }
}