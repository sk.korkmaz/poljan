﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.Models
{
    public class Settings
    {
        public bool HeadlessState { get; set; }
        public string Driver { get; set; }
        public string DriverVersion { get; set; }
        public int ImplicitTimeOut { get; set; }
        public int NumberOfAttempts { get; set; }

    }
}
