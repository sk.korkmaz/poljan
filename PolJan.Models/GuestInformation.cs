﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.Models
{
    public class GuestInformation
    {
        public string RoomNumber { get; set; }
        public string PassportNumber { get; set; }

        public string Name { get; set; }
        public string Surname { get; set; }
        public string FatherName { get; set; }
        public string MotherName { get; set; }

        public string BirthDate { get; set; }
        public string _BirthPlace;
        public string BirthPlace
        {
            get
            {
                if (String.IsNullOrEmpty(_BirthPlace))
                {
                    return ".";
                }
                else
                {
                    return _BirthPlace;
                }
            }
            set
            {
                _BirthPlace = value;
            }
        }
        public string Nation { get; set; }
        public string LicencePlateNumber { get; set; }
        private string _IdentityNumber;
        public string IdentityNumber
        {
            get
            {
                if (_IdentityNumber == "00000000000" || _IdentityNumber == ".")
                {
                    return "";
                }
                else
                {
                    return _IdentityNumber;
                }
            }
            set
            {
                _IdentityNumber = value;
            }
        }
        public string InOut { get; set; }
        public string CinDate { get; set; }
        public string CoutDate { get; set; }
        public string RoomChange { get; set; } = "";
        public string Sex { get; set; }
        public bool IsProcess { get; set; }
        public string KbsStateCode { get; set; }
        public string KbsStateText { get; set; }
        public string AsyaProfile { get; set; }
        public KbsState KbsState { get; set; }
        public NotificitaionState NotificitaionState{ get; set; }

        public ModelGuestType GetGuestType() 
        {
            if (String.IsNullOrEmpty(PassportNumber) && !String.IsNullOrEmpty(IdentityNumber))
            {
                return ModelGuestType.Local;
            }
            else if (!String.IsNullOrEmpty(PassportNumber) && String.IsNullOrEmpty(IdentityNumber))
            {
                return ModelGuestType.Foreign;
            }
            else if (!String.IsNullOrEmpty(PassportNumber) && !String.IsNullOrEmpty(IdentityNumber))
            {
                return ModelGuestType.Local;
            }
            return ModelGuestType.Foreign;
        }

        public string GetGuestKey() 
        {
            var guestType = GetGuestType();
            if (guestType == ModelGuestType.Foreign)
                return PassportNumber;
            else
                return IdentityNumber;

        }

        public bool CheckGuest() 
        {
            if(GetGuestType() == ModelGuestType.Local) 
            {
                if (IdentityNumber.Length!=11)
                {
                    SetState(KbsState.InformationError);
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if (String.IsNullOrEmpty(Name) ||
                    String.IsNullOrEmpty(Surname) ||
                    String.IsNullOrEmpty(Nation) ||
                    String.IsNullOrEmpty(BirthPlace) ||
                    String.IsNullOrEmpty(BirthDate) ||
                    String.IsNullOrEmpty(RoomNumber) ||
                    String.IsNullOrEmpty(PassportNumber)
                    )
                {
                    SetState(KbsState.InformationError);
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public void SetState(KbsState kbsState,string message = "") 
        {
            KbsState = kbsState;
            KbsStateCode = ((int)kbsState).ToString();
            switch (kbsState)
            {
                case KbsState.InformationError:
                    KbsStateText = "Bilgiler Eksik veya Yanlış";

                    break;
                case KbsState.Success:
                    KbsStateText = "İşlem Başarılı";
                    break;
                case KbsState.Error:
                    KbsStateText = "Bilinmeyen Bir Hata Oluştu (Bağlantı Hatası veya Servis Yok)";
                    break;
                case KbsState.Exist:
                    KbsStateText = "Misafir Tesiste Mevcut";
                    break;
                default:
                    break;
            }

        }

        public void SetNotificationState(NotificitaionState notificitaionState)
        {
            NotificitaionState = notificitaionState;
        }
        
    }

    public enum NotificitaionState 
    {
        Ready = 1,
        NotAvailable=2
    }

    public enum KbsState
    {
        InformationError = 1,
        Success = 2,
        Error = 3,
        Exist = 4,
    }

    public enum ModelGuestType 
    {
        Local = 1,
        Foreign = 2,
    }

    
}
