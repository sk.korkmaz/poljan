﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.Models
{
    public class TransactionResult
    {
        public TransactionResult(){}

        public TransactionResult(string resultText,bool resultState)
        {
            TransactionResultText = resultText;
            TransactionResultState = resultState;
        }
        public TransactionResult(TransactionResultType transactionResultType,string resultText = "") 
        {
            TransactionResultText = resultText;
            TransactionResultType = transactionResultType;
            switch (transactionResultType)
            {
                case TransactionResultType.ElementIsNotFound:
                    TransactionResultState = false;
                    break;
                case TransactionResultType.TransactionSuccessfull:
                    TransactionResultState = true;
                    break;
                case TransactionResultType.ElementIsNotFoundButSetSuccess:
                    TransactionResultState = true;
                    break;
                case TransactionResultType.TransactionError:
                    TransactionResultState = false;
                    break;
                default:
                    break;
            }
        }

        public string TransactionResultText { get; set; }
        public bool TransactionResultState { get; set; }
        public bool TransactionResultCode { get; set; }
        public TransactionResultType TransactionResultType { get; set; }
    }
    public enum TransactionResultType 
    {
        ElementIsNotFound = 1,
        TransactionSuccessfull = 2,
        TransactionError = 3,
        ElementIsNotFoundButSetSuccess = 4,
        ElementIsNotFoundButSetFalse = 5,
    }
}
