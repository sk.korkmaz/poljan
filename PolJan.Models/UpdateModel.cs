﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.Models
{

    public class UpdateModel
    {
        public bool update_ready { get; set; }
        public bool t_update { get; set; }
        public string date { get; set; }
        public string version { get; set; }
    }

}
