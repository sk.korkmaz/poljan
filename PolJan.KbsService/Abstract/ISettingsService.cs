﻿using PolJan.Models;

namespace PolJan.KbsService.Abstract
{
    public interface ISettingsService
    {
        Settings GetSettingsAsJson();
        void SaveSettingsAsJson(Settings settings);
    }
}