﻿using PolJan.Models;
using System.Collections.Generic;

namespace PolJan.KbsService.Abstract
{
    public interface IKbsEgmGuestGetService
    {
        List<GuestInformation> GetGuest();
        GuestInformation ExportGuest(GuestInformation guestInformations);
    }
}