﻿using PolJan.KbsService.Abstract;
using PolJan.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.KbsService
{
    public class KbsEgmGuestImportExportServiceFromFile: IKbsEgmGuestGetService
    {
        //2069900
        private string _FileKey;
        public string UserName = "";
        public string Password = "";
        public string IdentityNumber = "";
        public string Process = "";
        public KbsEgmGuestImportExportServiceFromFile(string FileKey)
        {
            _FileKey = FileKey;
            CreateReturnTxt(_FileKey);
        }

        public List<GuestInformation> GetGuest()
        {
            string[] getLines = File.ReadAllLines(SetReadingTxt(_FileKey), Encoding.Default);
            string[] splitLine;
            List<GuestInformation> _guestDetails = new List<GuestInformation>();
            foreach (string fLine in getLines)
            {
                GuestInformation guestDetails = new GuestInformation();
                splitLine = fLine.Split('|');
                UserName = splitLine[0].Trim(' ');
                Password = splitLine[1].Trim(' ');
                Process = splitLine[21].Trim(' ');
                IdentityNumber = splitLine[23].Trim(' ');
                //2
                guestDetails.IdentityNumber = splitLine[3].Trim(' ');
                guestDetails.PassportNumber = splitLine[4].Trim(' ');
                guestDetails.CinDate = splitLine[5].Trim(' ');
                guestDetails.CoutDate = splitLine[6].Trim(' ');
                //7
                guestDetails.RoomNumber = splitLine[8].Trim(' ');
                guestDetails.Name = splitLine[9].Trim(' ');
                guestDetails.Surname= splitLine[10].Trim(' ');
                guestDetails.MotherName = splitLine[11].Trim(' ');
                guestDetails.FatherName = splitLine[12].Trim(' ');
                guestDetails.BirthDate = splitLine[13].Trim(' ');
                guestDetails.BirthPlace = splitLine[14].Trim(' ');
                guestDetails.Nation = splitLine[15].Trim(' ');
                guestDetails.Sex = splitLine[16].Trim(' ') == "2201" ? "1" : "2";
                //17
                guestDetails.LicencePlateNumber = splitLine[18].Trim(' ');
                //19
                //20
                guestDetails.InOut = splitLine[21].Trim(' ');
                guestDetails.AsyaProfile = splitLine[22].Trim(' ');
                _guestDetails.Add(guestDetails);
            }

            return _guestDetails;
        }

        public void CreateReturnTxt(string parame)
        {
            File.Create(Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), parame + "kisidonus.txt")).Close();
        }

        public string SetReadingTxt(string parame)
        {
            //return "C:\\acucorp\\acucbl701\\acugt\\bin\\" + parame + "kisibilgi.txt";
            return Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), parame + "kisibilgi.txt");
        }

        public string GetReadingTxt(string parame) 
        {
            return Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), parame + "kisidonus.txt");

        }

        public string setWritingTxt(string parame)
        {
            return Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), parame + "kisidonus.txt");
        }

        public GuestInformation ExportGuest(GuestInformation _gd)
        {
            string b =
            " " + "|" +
            " " + "|" +
            " " + "|" +
            " " + "|" +
            _gd.PassportNumber + "|" +
                        _gd.IdentityNumber + "|" +
            " " + "|" +
            _gd.RoomNumber + "|" +
            _gd.Name + "|" +
            _gd.Surname + "|" +
            " " + "|" +
            " " + "|" +
            _gd.BirthDate + "|" +
            " " + "|" +
            " " + "|" +
            " " + "|" +
            " " + "|" +
            " " + "|" +
            " " + "|" +
            _gd.InOut + "|" +
            " " + "|" +
            _gd.KbsStateCode.ToString() + "|" +
            _gd.KbsStateText.ToString() + "|" +
            _gd.AsyaProfile.ToString() + "|" +
            Environment.NewLine;
            File.AppendAllText(setWritingTxt(_FileKey), b);
            return _gd;
        }
    }
}
