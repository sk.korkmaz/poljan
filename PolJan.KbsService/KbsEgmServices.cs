﻿using OpenQA.Selenium;
using PolJan.KbsService.Abstract;
using PolJan.Models;
using PolJan.WebDriver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.KbsService
{
    public class KbsEgmServices
    {
        private WebDriverOperations webDriverOperations;
        private IWebDriverOptions _webDriverOptions;
        private WebDriverApplication _webDriverApplication;
        private IKbsEgmGuestGetService _kbsEgmGuestGetService;
        public KbsEgmServices()
        {
            _webDriverOptions = new WebDriverOptions();
            switch (SettingsStatic.Driver)
            {
                case "Edge":
                    _webDriverApplication = WebDriverApplication.Edge;
                    break;
                case "Chrome":
                    _webDriverApplication = WebDriverApplication.Chrome;
                    break;
                case "Mozilla":
                    _webDriverApplication = WebDriverApplication.Mozilla;
                    break;
                default:
                    throw new Exception();
            }
            webDriverOperations = new WebDriverOperations(_webDriverApplication);
            webDriverOperations.SetImplicitTimeOut(SettingsStatic.ImplicitTimeOut);
        }


        public void CheckGuestForCommit(ref GuestInformation guestInformation) 
        {

            guestInformation.NotificitaionState = NotificitaionState.Ready;
            if (!guestInformation.CheckGuest())
            {
                guestInformation.SetState(KbsState.InformationError);
                guestInformation.SetNotificationState(NotificitaionState.NotAvailable);
                return;
            }
            var searchResult = CheckGuestIsExist(guestInformation.GetGuestKey(), guestInformation.GetGuestType());
            if (searchResult.TransactionResultType == TransactionResultType.TransactionSuccessfull)
            {
                guestInformation.SetState(KbsState.Exist);
                guestInformation.SetNotificationState(NotificitaionState.NotAvailable);
                return;
            }
            else if(searchResult.TransactionResultType == TransactionResultType.ElementIsNotFoundButSetFalse)
            {
                guestInformation.NotificitaionState = NotificitaionState.Ready;
            }
            else
            {
                guestInformation.SetState(KbsState.Error);
                guestInformation.SetNotificationState(NotificitaionState.NotAvailable);
            }

        }

        public void CheckInForeignGuest(ref GuestInformation guestInformation,bool CheckIfEcist = true) 
        {
            if (!GoToAddPage()) 
            {
                guestInformation.SetState(KbsState.Error);
                return;
            }
            var result = FillForignGuestDetails(guestInformation);
            if (result.TransactionResultState)
            {
                var saveForeignGuestResult = SaveForeignGuest();
                if (saveForeignGuestResult.TransactionResultType == TransactionResultType.TransactionSuccessfull)
                {
                    guestInformation.SetState(KbsState.Success, saveForeignGuestResult.TransactionResultText);
                }

                else if (saveForeignGuestResult.TransactionResultType == TransactionResultType.TransactionError)
                { guestInformation.SetState(KbsState.InformationError, saveForeignGuestResult.TransactionResultText); }
                else
                { guestInformation.SetState(KbsState.Error, saveForeignGuestResult.TransactionResultText); }
            }
            else
            { guestInformation.SetState(KbsState.Error, result.TransactionResultText); }
        }

        public void CheckInLocalGuest(ref GuestInformation guestInformation,bool CheckIfExist = true)
        {
 
            var resultMernis = MernisQuery(guestInformation.IdentityNumber);
            if (resultMernis)
            {
                var resultSave = SaveLocalGuest(guestInformation.RoomNumber);
                if (resultSave.TransactionResultState)
                { guestInformation.SetState(KbsState.Success, resultSave.TransactionResultText); }
                else
                { guestInformation.SetState(KbsState.Error, resultSave.TransactionResultText); }
            }
            else
            { guestInformation.SetState(KbsState.InformationError); }
        }

        public void CheckCommit(ref GuestInformation guestInformation) 
        {
            if (!GoToAddPage())
            {
                return;
            }

            var LastCheckresult = CheckGuestIsExist(guestInformation.GetGuestKey(), guestInformation.GetGuestType());
            if (LastCheckresult.TransactionResultType == TransactionResultType.ElementIsNotFoundButSetFalse)
            {
                guestInformation.SetState(KbsState.InformationError);
            }
            else if(LastCheckresult.TransactionResultType == TransactionResultType.ElementIsNotFound)
            {
                guestInformation.SetState(KbsState.Error);
            }
            else if (LastCheckresult.TransactionResultType == TransactionResultType.TransactionSuccessfull)
            {
                guestInformation.SetState(KbsState.Success);

            }
            else
            {
                guestInformation.SetState(KbsState.Error);

            }

        }

        public void TerminateProcess() 
        {
            webDriverOperations.TerminateDriver();
        }
        public bool Login(string userName,string password,string identityNumber)
        {

            var navigateResult = webDriverOperations.NavigateToUrl("https://kbs.egm.gov.tr/");
            var signUrl = webDriverOperations.GetCurrentUrl();
            if (!navigateResult)
                return false;
            //var userNameField = webDriverOperations.GetElement("/html/body/div[2]/div/div/div/div[2]/form/fieldset/div[2]/input", true);
            //var passwordField = webDriverOperations.GetElement("/html/body/div[2]/div/div/div/div[2]/form/fieldset/div[3]/input", true);
            //var submitButton = webDriverOperations.GetElement("/html/body/div[2]/div/div/div/input", true);            

            var userNameField = webDriverOperations.GetElement("/html/body/table[2]/tbody/tr[2]/td/form/table/tbody/tr[3]/td/input", true);
            var passwordField = webDriverOperations.GetElement("/html/body/table[2]/tbody/tr[2]/td/form/table/tbody/tr[5]/td/input", true);
            var identityField = webDriverOperations.GetElement("/html/body/table[2]/tbody/tr[2]/td/form/table/tbody/tr[4]/td/input", true);
            var submitButton = webDriverOperations.GetElement("/html/body/table[2]/tbody/tr[2]/td/form/table/tbody/tr[6]/td/input", true);

            if (userNameField == null && passwordField == null && identityField == null)
            {
                return false;
            }
            userNameField.SendKeys(userName);
            passwordField.SendKeys(password);
            identityField.SendKeys(identityNumber);
            webDriverOperations.ClickElement(submitButton);

            var checkField = webDriverOperations.GetElement("/html/body/table[2]/tbody/tr/td/form/table/tbody/tr[3]/td/label", true);
            if (checkField == null)
            {
                return false;
            }
            if (checkField.Text == "Tek Kullanımlık Parola")
            {
                webDriverOperations.ExecuteJavascript("document.evaluate('/html/body/table[2]/tbody/tr/td/form/table/tbody/tr[5]/td/input', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.style.visibility = 'hidden';");

                return true;
            }
            else
            {
                return false;
            }
            //webDriverOperations.NavigateToUrl("https://kbs.egm.gov.tr/ProjeDosya/konaklayanekle.aspx");
            //if (signUrl != webDriverOperations.GetCurrentUrl())
            //{
            //    webDriverOperations.ExecuteJavascript("document.getElementById('btnSubmit_11').style.visibility = 'hidden';");

            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }
        public bool SecondaryLogin()
        {
            webDriverOperations.ExecuteJavascript("document.evaluate('/html/body/table[2]/tbody/tr/td/form/table/tbody/tr[5]/td/input', document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.style.visibility = 'visible';");
            var submitButton = webDriverOperations.GetElement("/html/body/table[2]/tbody/tr/td/form/table/tbody/tr[5]/td/input", true);
            webDriverOperations.ClickElement(submitButton);

            if (webDriverOperations.GetCurrentUrl() == "https://kbs.egm.gov.tr/ProjeDosya/homepage.aspx")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void CheckOutGuest(ref GuestInformation guestInformation)
        {
            var checkoutResult = CheckOut(guestInformation.GetGuestKey(), guestInformation.GetGuestType());
            if (checkoutResult.TransactionResultType == TransactionResultType.TransactionSuccessfull)
            {
                guestInformation.SetState(KbsState.Success);
            }
            else if(checkoutResult.TransactionResultType == TransactionResultType.ElementIsNotFoundButSetFalse)
            {
                guestInformation.SetState(KbsState.InformationError);
            }
            else if(checkoutResult.TransactionResultType == TransactionResultType.TransactionError) 
            {
                guestInformation.SetState(KbsState.Error);
            }
        }

        private TransactionResult CheckOut(string key,ModelGuestType guestType) 
        {
            var searchResult = CheckGuestIsExist(key, guestType);
            if (searchResult.TransactionResultType == TransactionResultType.TransactionSuccessfull)
            {
                var getCoutButton = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[4]/table/tbody/tr[2]/td[11]/input");
                if (getCoutButton == null)
                    return new TransactionResult(TransactionResultType.ElementIsNotFound);

                webDriverOperations.ClickElement(getCoutButton);
                //var popUp = webDriverOperations.GetElement("/html/body/div[4]/div[2]/div/div/div/div/div[4]/button[1]");
                var popUp = webDriverOperations.GetElement("/html/body/div[4]/div[2]/div/div/div/div/div[4]/button[1]");
                if (popUp != null)
                    webDriverOperations.ClickElement(popUp);
                else
                    return new TransactionResult(TransactionResultType.ElementIsNotFound);
                var alertState = webDriverOperations.GetAlert(2);
                if (alertState != null) 
                {
                    alertState.Accept();
                    return new TransactionResult(TransactionResultType.TransactionSuccessfull);

                }
                else
                    return new TransactionResult(TransactionResultType.ElementIsNotFoundButSetFalse);
            }
            else
            {
                return new TransactionResult(TransactionResultType.ElementIsNotFoundButSetFalse);
            }
        }

        public bool MernisQuery(string identityNumber = "111111111111111") 
        {
            if (webDriverOperations.GetCurrentUrl() == "https://kbs.egm.gov.tr/ProjeDosya/konaklayanekle.aspx")
            {
                var identityInput = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[1]/div/div/div/div[1]/div[1]/input[1]", true);
                var queryButton = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[1]/div/div/div/div[1]/div[1]/input[2]", true);

                if (identityInput == null && queryButton == null)
                    return false;
                identityInput.SendKeys(identityNumber);
                webDriverOperations.ClickElementWithJavascript(queryButton);
                var resultElement = webDriverOperations.GetElement("/html/body/section/form/div[2]/div[2]");
                if (resultElement == null)
                    return true;
                else
                    return false;
            }
            else
            {
                return false;
            }
        }

        public TransactionResult CheckSaveAlert() 
        {
            var alert = webDriverOperations.GetAlert(2);
            if (alert != null)
            {
                if (alert.Text == "Konaklayan Kişisi Başarı İle Kaydedildi.!.." || alert.Text == "Konaklayan Kişisi Halen Tesisinizde Mevcut!...")
                {
                    alert.Accept();
                    return new TransactionResult(TransactionResultType.TransactionSuccessfull);
                }
                else
                {
                    return new TransactionResult(TransactionResultType.TransactionError);
                }
            }
            else
            {
                return new TransactionResult(TransactionResultType.ElementIsNotFound);
            }
        }

        public TransactionResult CommonSaveOperation(ModelGuestType guestType) 
        {
            var alertResult = CheckSaveAlert();
            if (alertResult.TransactionResultState)
            {
                return new TransactionResult(TransactionResultType.TransactionSuccessfull);
            }
            else
            {
                //check here later

                IWebElement popupConfirmButton = webDriverOperations.GetElement("/html/body/div[3]/div[2]/div/div/div/div/div[4]/button[1]");
                IWebElement popupConfirmButtonAlternative = webDriverOperations.GetElement("/html/body/div[2]/div[2]/div/div/div/div/div[4]/button[1]");
                if (popupConfirmButtonAlternative == null)
                {
                    popupConfirmButtonAlternative = webDriverOperations.GetElement("/html/body/div[1]/div[2]/div/div/div/div/div[4]/button[1]");

                }
                if (popupConfirmButton == null && popupConfirmButtonAlternative == null)
                {
                    if (guestType == ModelGuestType.Foreign)
                    {
                        var validationSummaryElement = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[4]/div");
                        var popupValidationSummary = webDriverOperations.GetElement("/html/body/section/form/div[2]");
                        if (validationSummaryElement != null || popupValidationSummary != null)
                        {
                            return new TransactionResult(TransactionResultType.TransactionError);//misafir bilgileri eksik
                        }
                        else
                        {
                            return new TransactionResult(TransactionResultType.TransactionError);//tanımsız hata
                        }
                    }
                    else
                    {
                        return new TransactionResult(TransactionResultType.ElementIsNotFoundButSetFalse);
                    }

                }
                else
                {
                    if (popupConfirmButton != null && popupConfirmButton.Text == "TAMAM")
                    {
                        webDriverOperations.ClickElement(popupConfirmButton);
                    }
                    else if (popupConfirmButtonAlternative != null && popupConfirmButtonAlternative.Text == "TAMAM")
                    {
                        webDriverOperations.ClickElement(popupConfirmButtonAlternative);

                    }
                    var alertAfterPopup = CheckSaveAlert();
                    if (alertAfterPopup.TransactionResultType == TransactionResultType.TransactionSuccessfull)
                    {
                        return new TransactionResult(TransactionResultType.TransactionSuccessfull,"Misafir Kaydedildi");

                    }
                    else
                    {
                        return new TransactionResult(TransactionResultType.TransactionError);
                    }

                }
            }
        
        }
        public TransactionResult CheckGuestIsExist(string key,ModelGuestType guestType)
        {
            IWebElement searchInputElement = null;
            switch (guestType)
            {
                case ModelGuestType.Local:
                    searchInputElement = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[3]/table/tbody/tr/td[1]/input");
                    break;
                case ModelGuestType.Foreign:
                    searchInputElement = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[3]/table/tbody/tr/td[2]/input");
                    break;
                default:
                    break;

            }
            if (searchInputElement == null)
            {
                return new TransactionResult(TransactionResultType.ElementIsNotFound);
            }
            else
            {
                searchInputElement.SendKeys(key);
                IWebElement searchButtonElement = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[3]/div[1]/input");
                if (searchButtonElement != null)
                {
                    webDriverOperations.ClickElement(searchButtonElement); 
                    var searchResult = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[4]/table/tbody/tr[2]");
                    if (searchResult == null)
                    {
                        return new TransactionResult(TransactionResultType.ElementIsNotFoundButSetFalse);
                    }
                    else
                    {
                        return new TransactionResult(TransactionResultType.TransactionSuccessfull);
                    }
                }
                else
                {
                    return new TransactionResult(TransactionResultType.ElementIsNotFound);
                }
            }
        }

        public TransactionResult SaveLocalGuest(string roomNumber) 
        {
            //this method must be bind with MernisQuery return state.
            var roomNumberElement = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[1]/div/div/div/div[2]/div[5]/input",true);
            var saveButton = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[1]/div/div/div/div[2]/div[14]/input", true);
            if (saveButton != null && roomNumber != null)
            {
                roomNumberElement.SendKeys(roomNumber);
                saveButton.Click();
                return CommonSaveOperation(ModelGuestType.Local);
            }
            else
            {
                return new TransactionResult(TransactionResultType.TransactionError);
            }
        }

        public bool GoToAddPage() 
        {
           return webDriverOperations.NavigateToUrl("https://kbs.egm.gov.tr/ProjeDosya/konaklayanekle.aspx");
        }
        public TransactionResult FillForignGuestDetails(GuestInformation guestInformation) 
        {
            List<IWebElement> webElements = new List<IWebElement>();
            var foreignInputPlace = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/ul/li[2]/a", true);

            if (foreignInputPlace == null)
                return new TransactionResult(TransactionResultType.ElementIsNotFound,"foreignInputPlace Not Found");
            webDriverOperations.ClickElementWithJavascript(foreignInputPlace);
            var sexInputElement = "/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[1]/div[3]/select";
            var nationInputElement = "/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/div[2]/select";
            var documentNo = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[1]/div[1]/input", true);
            if (documentNo == null)
                return new TransactionResult(TransactionResultType.ElementIsNotFound,"documentNo not found"); //logically if this element is exist other element must will exist

            webElements.Add(documentNo);

            var name = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[1]/div[2]/input", true);
            webElements.Add(name);

            var surname = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[1]/div[4]/input", true);
            webElements.Add(surname);

            var birthDate = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[1]/div[7]/input", true);
            webElements.Add(birthDate);

            var birthPlace = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/div[1]/input", true);
            webElements.Add(birthPlace);

            var roomNumber = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[2]/div[5]/input", true);
            webElements.Add(roomNumber);

            if (webElements.Contains(null))
                return new TransactionResult(TransactionResultType.ElementIsNotFound,"fill elements is not load");

            documentNo.SendKeys(guestInformation.PassportNumber);
            name.SendKeys(guestInformation.Name);
            surname.SendKeys(guestInformation.Surname);
            webDriverOperations.ExecuteJavascript("document.getElementById('txtYDogum').value='" + DateTime.Parse(guestInformation.BirthDate).ToString("dd.MM.yyyy") + "'"); //because datetime format
            birthPlace.SendKeys(guestInformation.BirthPlace);
            roomNumber.SendKeys(guestInformation.RoomNumber);
            webDriverOperations.SetDropdownValue(sexInputElement, guestInformation.Sex);
            try
            {
                var nationCode = Convert.ToInt32(guestInformation.Nation);
                webDriverOperations.SetDropdownValue(nationInputElement, nationCode.ToString());

            }
            catch { webDriverOperations.SetDropdownValue(nationInputElement, "250");  }

            return new TransactionResult(TransactionResultType.TransactionSuccessfull);

        }
        public TransactionResult SaveForeignGuest() 
        {
            var saveBtnElement = webDriverOperations.GetElement("/html/body/section/form/div[3]/div/div/div/div[2]/div[2]/div/div[2]/div/div/div[1]/div[5]/input", true);
            if (saveBtnElement!= null)
            {
                var clickResult = webDriverOperations.ClickElement(saveBtnElement);
                if (clickResult)
                {
                    return CommonSaveOperation(ModelGuestType.Foreign);
                }
                else
                {
                    return new TransactionResult(TransactionResultType.ElementIsNotFound);
                }
            }
            else
            {
                return new TransactionResult(TransactionResultType.ElementIsNotFound);
            }
        }
    }
}
