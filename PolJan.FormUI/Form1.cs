﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using PolJan.KbsService;
using PolJan.KbsService.Abstract;
using PolJan.Models;
using PolJan.WebDriver;
namespace PolJan.FormUI
{
    public partial class Form1 : Form
    {
        private string _FileKey = "";
        //test-branch
        //test2-branch
		//test-3
		//test-4
        public Form1(string FileKey)
        {
            CommonInitialize();
            _FileKey = FileKey;
             kbsEgmGuestGetService = new KbsEgmGuestImportExportServiceFromFile(_FileKey);
        }
        public Form1() 
        {
            CommonInitialize();
            _FileKey = "2069900";
            kbsEgmGuestGetService = new KbsEgmGuestImportExportServiceFromFile(_FileKey);

        }

        public void CommonInitialize()
        {
            InitializeComponent();
            this.Load += Form1_Load;
        }

        public void SetLabelHeader(string processType,string key) 
        {
            this.BeginInvoke((MethodInvoker)delegate
            {

                parameterTxt.Text = key;
                processTypeTxt.Text = processType;

            });
        }

        public void SetCustomLabelText(Label label,string text) 
        {
            this.BeginInvoke((MethodInvoker)delegate
            {
                label.Text = text;
            });
        }
        public void SetLabelsText(GuestInformation guestInformation) 
        {
            this.BeginInvoke((MethodInvoker)delegate
            {
                nameLbl.Text = guestInformation.Name;
                surnameLbl.Text = guestInformation.Surname;
                idNumberLbl.Text = guestInformation.GetGuestKey();
                roomNumberLbl.Text = guestInformation.RoomNumber;

            });
        }

        public Version GetVersion() 
        {

            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
            string version = fvi.FileVersion;

            return new Version(version);

            //if (!File.Exists(Path.Combine(Application.StartupPath,"ver.asya")))
            //{
            //    MessageBox.Show("Version Kontrolü Yapılamadı","Hata", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    throw new FileNotFoundException("file not found [ver.asya]");
            //}
            //else
            //{
            //    var version = File.ReadAllLines(Path.Combine(Application.StartupPath, "ver.asya"));
            //    if (version.Length > 0)
            //    {
            //        var versionRow = version[0].ToString().Replace("[","").Replace("]","");
            //        return new Version(versionRow);
            //    }
            //    else
            //    {
            //        throw new Exception("version is not found. please check ver.asya file content");
            //    }
            //}
        }

        string userName = "";
        string password = "";
        string process = "";
        string identity = "";
        bool loginState = false;
        IKbsEgmGuestGetService kbsEgmGuestGetService;
        KbsEgmServices kbsEgmServices = null;

        List<GuestInformation> guestInformationList = new List<GuestInformation>();

        private async void Form1_Load(object sender, EventArgs e)
        {
            Rectangle workingArea = Screen.GetWorkingArea(this);
            this.Location = new Point(workingArea.Right - Size.Width,
                                      workingArea.Bottom - Size.Height);
            string versionString = "";
            versionLbl.Text = GetVersion().ToString();
            var http = new HttpClient();
            //try
            //{
            //   versionString  = await http.GetStringAsync(new Uri("http://31.7.33.69:81/asyakur/Poljan/poljan-update-settings.json"));
            //   if (!String.IsNullOrEmpty(versionString))
            //   {
            //       var resultUpdateModel = JsonConvert.DeserializeObject<UpdateModel>(versionString);
            //       Version Lastversion = new Version(resultUpdateModel.version);
            //       var currentVersion = GetVersion();
            //       if (Lastversion > currentVersion && resultUpdateModel.update_ready)
            //       {
            //           UpdateForm updateForm = new UpdateForm(resultUpdateModel);
            //           var state = updateForm.ShowDialog();
            //           if (state != DialogResult.Cancel)
            //           {
            //               return;
            //           }
            //       }
            //   }
            //}
            //catch{}

            await Task.Run(() =>
            {
                try
                {
                    kbsEgmServices = new KbsEgmServices();

                }
                catch (InvalidOperationException ex)
                {
                    MessageBox.Show("Sürücü Oluşturulurken Hata Oluştu. Versiyon Güncellemesi Yapınız" + Environment.NewLine + Environment.NewLine + ex.Message.ToString());
                    TerminateAllExe();
                }

                guestInformationList = kbsEgmGuestGetService.GetGuest();

                userName = ((KbsEgmGuestImportExportServiceFromFile)kbsEgmGuestGetService).UserName;
                password = ((KbsEgmGuestImportExportServiceFromFile)kbsEgmGuestGetService).Password;
                process = ((KbsEgmGuestImportExportServiceFromFile)kbsEgmGuestGetService).Process;
                identity = ((KbsEgmGuestImportExportServiceFromFile)kbsEgmGuestGetService).IdentityNumber;
                SetLabelHeader(process,_FileKey);
                SetCustomLabelText(monitorLbl, "Login İşlemi Gerçekleştiriliyor.");
                loginState = kbsEgmServices.Login(userName, password,identity);
                if (loginState)
                {
                    this.BeginInvoke((MethodInvoker)delegate { continueProcessBtn.Visible = true; });
                    //continueProcessBtn.Visible = true;
                }
            });

            //TerminateAllExe();
            
        }

        public void TerminateAllExe()
        {


            kbsEgmServices.TerminateProcess();
            Application.Exit();

        }

        private void terminateProcess_Click(object sender, EventArgs e)
        {
            this.BeginInvoke((MethodInvoker)delegate { TerminateAllExe(); });
        }

        private void minimizeBtn_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private async void continueProcessBtn_Click(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                if (loginState)
                {
                    var secondaryLoginResult = kbsEgmServices.SecondaryLogin();
                    if (!secondaryLoginResult)
                    {
                        MessageBox.Show("Login Başarısız Lütfen Tekrar Deneyiniz");
                        this.BeginInvoke((MethodInvoker)delegate { TerminateAllExe(); });
                        return;

                    }
                    SetCustomLabelText(monitorLbl, "Login Başarılı");

                    if (process == "GIRIS")
                    {

                        //SetCustomLabelText(monitorLbl, "Misafirler Kontrol Ediliyor");

                        //foreach (var item in guestInformationList)
                        //{
                        //    SetLabelsText(item);

                        //    GuestInformation TempGuestInformation = item;
                        //    kbsEgmServices.CheckGuestForCommit(ref TempGuestInformation);
                        //}
                        SetCustomLabelText(monitorLbl, "Check In Bildirim İşlemi Başladı");

                        foreach (var guest in guestInformationList)
                        {
                            try
                            {
                                kbsEgmServices.GoToAddPage();

                                var TempGuestInformation = guest;
                                kbsEgmServices.CheckGuestForCommit(ref TempGuestInformation);
                                kbsEgmServices.GoToAddPage();

                                SetLabelsText(TempGuestInformation);
                                if (TempGuestInformation.NotificitaionState == NotificitaionState.Ready)
                                {
                                    if (TempGuestInformation.GetGuestType() == ModelGuestType.Foreign)
                                        kbsEgmServices.CheckInForeignGuest(ref TempGuestInformation);
                                    else
                                        kbsEgmServices.CheckInLocalGuest(ref TempGuestInformation);

                                    SetCustomLabelText(monitorLbl, "Bildirim İşlemi Kontrol Ediliyor");
                                    kbsEgmServices.CheckCommit(ref TempGuestInformation);
                                }
                                kbsEgmGuestGetService.ExportGuest(TempGuestInformation);
                            }
                            catch (Exception ex)
                            {

                                throw ex;
                            }

                        }
                    }
                    else if (process == "CIKIS")
                    {
                        SetCustomLabelText(monitorLbl, "Check Out Bildirim İşlemi Başladı");
                        kbsEgmServices.GoToAddPage();

                        foreach (var guest in guestInformationList)
                        {
                            GuestInformation tempGuest = guest;
                            SetLabelsText(tempGuest);
                            kbsEgmServices.CheckOutGuest(ref tempGuest);
                            kbsEgmGuestGetService.ExportGuest(tempGuest);
                        }
                    }
                    else if (process == "SORGU")
                    {
                        //sorgu gelecek
                    }
                    Process.Start(((KbsEgmGuestImportExportServiceFromFile)kbsEgmGuestGetService).GetReadingTxt(_FileKey));

                    //kbsEgmServices.TerminateProcess();
                    this.BeginInvoke((MethodInvoker)delegate { TerminateAllExe(); });
                    return;
                }
                else
                {
                    SetCustomLabelText(monitorLbl, "Giriş Yapılamadı. Tekrar Deneyiniz.");

                    foreach (var guest in guestInformationList)
                    {
                        guest.SetState(KbsState.Error, "Login Olunamadı");
                        kbsEgmGuestGetService.ExportGuest(guest);
                    }
                    MessageBox.Show("Giriş Sağlanamadı. İnternet Bağlantınızı Kontrol Ediniz. " + Environment.NewLine + Environment.NewLine + "Servis Bağlantısında Hata Oluştu. Tekrar Deneyiniz");
                    TerminateAllExe();
                }
            });

        }
    }

}
