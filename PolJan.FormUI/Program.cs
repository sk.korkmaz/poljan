﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolJan.FormUI
{
    static class Program
    {
        private static string keyParam = "";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] param)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += Application_ThreadException;
            //param = new string[] { "2069900" };
            if (param.Length > 0)
            {
                keyParam = param[0].ToString();
                Application.Run(new Form1(keyParam));

            }
            else
            {
                Application.Run(new Form1());

            }

        }

        private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {
            //throw new NotImplementedException();
        }
    }
}
