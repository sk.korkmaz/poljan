﻿using PolJan.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PolJan.FormUI
{
    public partial class UpdateForm : Form
    {
        UpdateModel _updateModel;
        public UpdateForm(UpdateModel updateModel)
        {
            InitializeComponent();
            _updateModel = updateModel;
            this.Load += UpdateForm_Load;
        }

        private void UpdateForm_Load(object sender, EventArgs e)
        {
            dateLbl.Text = _updateModel.date;
            versionLbl.Text = _updateModel.version;
        }

        private void updateBtn_Click(object sender, EventArgs e)
        {
            updateBtn.Enabled = false;
            updateBtn.Text = "Güncelleme İşlemi Devam Ediyor... Lütfen Bekleyiniz...";
            Download();
            
            //Process.Start("http://31.7.33.69:81/asyakur/Poljan/AsyasoftPolJan.msi");
            //Application.Exit();
        }
        public async Task Download() 
        {
            string remoteUri = "http://31.7.33.69:81/Poljan/";
            string fileName = "update-"+ _updateModel.version +".zip", 
                myStringWebResource = null;
            WebClient myWebClient = new WebClient();
            myStringWebResource = remoteUri + fileName;
            Console.WriteLine("Downloading File \"{0}\" from \"{1}\" .......\n\n", fileName, myStringWebResource);
            await myWebClient.DownloadFileTaskAsync(myStringWebResource, fileName);
            var processResult = Process.Start("PolJan.Update.Console.exe",_updateModel.version);
            Application.Exit();

        }
        private void button1_Click_1(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
