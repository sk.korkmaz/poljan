﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.WebDriver
{
    public static class SettingsStatic
    {
        public static bool HeadlessState { get; set; }
        public static string Driver { get; set; }
        public static string DriverVersion { get; set; }
        public static int ImplicitTimeOut { get; set; }
        public static int NumberOfAttempts { get; set; }
        public static int PageLoadTimeout { get; set; }
    }
}
