﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.WebDriver
{
    public class SettingsService:ISettingsService
    {
        private string settingsFilePath = "";
        private Settings _settings;
        public SettingsService()
        {
            settingsFilePath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), "settings.json"); 
        }

        public void GetSettingsAsJson() 
        {
            if (File.Exists(settingsFilePath))
            {
                var jsonData = File.ReadAllText(settingsFilePath);
                _settings = new Settings();
                _settings = JsonConvert.DeserializeObject<Settings>(jsonData);
                _settings.MapToStaticClass();
            }
            else
            {
                throw new FileNotFoundException("settings file is not found");
            }
        }

        public void SaveSettingsAsJson(Settings settings)
        {
            if (File.Exists(settingsFilePath))
            {
                var jsonData = JsonConvert.SerializeObject(settings);
                File.WriteAllText(settingsFilePath, "");
                if (!string.IsNullOrEmpty(jsonData))
                {
                    File.WriteAllText(settingsFilePath, jsonData);
                }
            }
        }
    }
}
