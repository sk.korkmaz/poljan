﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Edge.SeleniumTools;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace PolJan.WebDriver
{
    public interface IWebDriverOptions
    {
        EdgeOptions GetEdgeDriverOptions();
        ChromeOptions GetChromeDriverOptions();
        ChromeDriverService GetChromeDriverService();
    }
}
