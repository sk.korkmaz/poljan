﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.WebDriver
{

    public delegate void WebDriverOperationsDelegate(object source, WebDriverOperationsEventArgs webDriverOperationsEventArgs);
    public class WebDriverOperations
    {
        public event WebDriverOperationsDelegate ExceptionHandled;
        private WebDriverContext webDriverContext;
        private ITimeOut timeOut;
        public WebDriverOperations(WebDriverApplication webDriverApplication)
        {
            WebDriverContext.WebDriverApplication = webDriverApplication;
            webDriverContext = WebDriverContext.Instance;
            timeOut = new TimeOut();
        }

        public void TerminateDriver() 
        {
            webDriverContext.GetWebDriver.Close();
            webDriverContext.GetWebDriver.Quit();
        }

        public bool ElementIsInteractable(IWebElement webElement) 
        {
            while (true)
            {
                if (webElement != null && webElement.Displayed && webElement.Enabled)
                {
                    return true;
                }
            }
        }

        private IWebElement WaitUntilAndGetElement(string xpath, int seconds = 10)
        {
            try
            {
                
                WebDriverWait wait = new WebDriverWait(webDriverContext.GetWebDriver, timeout: TimeSpan.FromSeconds(seconds))
                {
                    PollingInterval = TimeSpan.FromSeconds(1),
                };
                wait.IgnoreExceptionTypes(typeof(NoSuchElementException));

                var element = wait.Until(drv => drv.FindElement(By.XPath(xpath)));
                //wait.Until(ExpectedConditions.ElementIsVisible(By.XPath(xpath)));
                DefaultWait<IWebElement> defaultWait = new DefaultWait<IWebElement>(element);
                defaultWait.IgnoreExceptionTypes(typeof(ElementNotInteractableException));
                defaultWait.Until(ElementIsInteractable);
                return element;
            }
            catch (WebDriverTimeoutException)
            {
                return null;
            }


        }

        public string getVersion() 
        {
            ICapabilities capabilities = ((RemoteWebDriver)webDriverContext.GetWebDriver).Capabilities;
            var result = (capabilities.GetCapability("chrome") as Dictionary<string, object>)["chromedriverVersion"];
            return result.ToString();
        }

        public void ExecuteJavascript(string javascript) 
        {
            IJavaScriptExecutor ex = (IJavaScriptExecutor)webDriverContext.GetWebDriver;
            ex.ExecuteScript(javascript);
        }

        public IAlert GetAlert(int timeout = 10) 
        {
            WebDriverWait wait = new WebDriverWait(webDriverContext.GetWebDriver, timeout: TimeSpan.FromSeconds(timeout))
            {
                PollingInterval = TimeSpan.FromSeconds(1)
            };
            wait.IgnoreExceptionTypes(typeof(NoSuchElementException));
            try
            {
                IAlert alert = wait.Until(ExpectedConditions.AlertIsPresent());
                if (alert == null)
                {
                    return null;
                }
                else
                {
                    return alert;
                }
            }
            catch
            {
                return null;
            }

        }

        public IWebElement GetElement(string xpath, bool WaitUntilExist = false)
        {

            if (WaitUntilExist)
            {
                return WaitUntilAndGetElement(xpath);
            }
            else
            {
                try
                {
                    return webDriverContext.GetWebDriver.FindElement(By.XPath(xpath));
                }
                catch
                {
                    return null;
                }
            }

        }
        public IWebElement GetElementById(string id) 
        {
            return webDriverContext.GetWebDriver.FindElement(By.Id(id));
        }
        public IWebElement GetElementByClassName(string className)
        {
            return webDriverContext.GetWebDriver.FindElement(By.ClassName(className));

        }
        public SelectElement GetSelectElement(string xpathDropdown) 
        {
            var dropDownElement = GetElement(xpathDropdown, true);
            var options = new SelectElement(dropDownElement);
            return options;
        }

        public bool SetDropdownValue(string xpathDropDown,string data,bool byValue=true) 
        {
            var options = GetSelectElement(xpathDropDown);
            if (options == null)
                return false;
            try
            {
                if (byValue)
                {
                    options.SelectByValue(data);
                    return true;
                }
                else
                {
                    options.SelectByText(data);
                    return true;
                }
            }
            catch { return false; }

        }

        public bool NavigateToUrl(string url,int attemptSecond = 10)
        {
            var endTime = timeOut.Later(TimeSpan.FromSeconds(10));
            while (true)
            {
                try
                {
                    if (!this.timeOut.IsTimeOut(endTime))
                    {
                        return false;
                    }
                    webDriverContext.GetWebDriver.Navigate().GoToUrl(url);
                    return true;

                }
                catch{}
            }

        }

        public bool ClickElement(string xPath)
        {
            var element = GetElement(xPath, true);
            if (element != null)
            {
                element.Click();
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool ClickElement(IWebElement webElement) 
        {
            try
            {
                webElement.Click();
                return true;
            }
            catch
            {

                return false;
            }
        }

        public void ClickElementWithJavascript(IWebElement webElement) 
        {
            IJavaScriptExecutor ex = (IJavaScriptExecutor)webDriverContext.GetWebDriver;
            ex.ExecuteScript("arguments[0].click();", webElement);
        }

        public string GetAttributeFromElement(string xpath, string attribute)
        {
            var element = WaitUntilAndGetElement(xpath);
            if (element != null)
            {
                return element.GetAttribute(attribute);
            }
            else
            {
                return "";
            }
        }

        public void SendKeysToElement(string xpath, string value)
        {
            var element = WaitUntilAndGetElement(xpath);
            if (element != null)
            {
                element.SendKeys(value);
            }
            else
            {
                throw new NoSuchElementException();
            }
        }

        public void SendKeysToElement(Dictionary<string, string> elementsXpathAndValues)
        {
            if (elementsXpathAndValues.Count() > 0)
            {
                foreach (var item in elementsXpathAndValues)
                {
                    var element = WaitUntilAndGetElement(item.Key);
                    if (element != null)
                    {
                        element.SendKeys(item.Value);
                    }
                    else
                    {
                        continue;
                    }
                }
            }
        }
        public void SetImplicitTimeOut(int timeOut = 15)
        {
            webDriverContext.GetWebDriver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeOut);
        }

        public string GetCurrentUrl() 
        {
            return webDriverContext.GetWebDriver.Url;
        }
        public void Stop()
        {
            webDriverContext.GetWebDriver.Quit();
        }
    }
    
    public class WebDriverOperationsEventArgs : EventArgs 
    {
        public IWebElement webElement { get; set; }
        public string ExceptionMessage { get; set; }
    }
}
