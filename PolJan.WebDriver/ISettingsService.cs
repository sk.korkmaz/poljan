﻿namespace PolJan.WebDriver
{
    public interface ISettingsService
    {
        void GetSettingsAsJson();
        void SaveSettingsAsJson(Settings settings);
    }
}