﻿using OpenQA.Selenium.Edge;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Edge.SeleniumTools;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace PolJan.WebDriver
{
    public class WebDriverOptions : IWebDriverOptions
    {
        ISettingsService _settingsService;
        public WebDriverOptions()
        {

            _settingsService = new SettingsService();
            _settingsService.GetSettingsAsJson();
        }

        public ChromeOptions GetChromeDriverOptions()
        {
            var chromeOptions = new ChromeOptions();
            if (SettingsStatic.HeadlessState)
            {
                chromeOptions.AddArguments(new List<string>() {
                "--silent-launch",
                "--no-startup-window",
                "no-sandbox",
                "headless"});
            }
            else
            {
                chromeOptions.AddArgument("start-maximized");
            }


            return chromeOptions;
        }
        public ChromeDriverService GetChromeDriverService() 
        {
            var driverService = ChromeDriverService.CreateDefaultService();

            if (SettingsStatic.HeadlessState) 
            {
                driverService.HideCommandPromptWindow = true;
            }

            return driverService;

        }

        public Microsoft.Edge.SeleniumTools.EdgeOptions GetEdgeDriverOptions()
        {
            var options = new Microsoft.Edge.SeleniumTools.EdgeOptions();
            options.UseChromium = true;
            if (SettingsStatic.HeadlessState) 
            {
                
                options.AddArguments("headless", "--silent-launch", "--no-startup-window", "no-sandbox");

            }

            //options.AddExtension("Block-image_v1.0.crx");

            return options;
        }
    }
}
