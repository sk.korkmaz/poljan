﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.WebDriver
{
    public class Settings
    {
        public bool HeadlessState { get; set; }
        public string Driver { get; set; }
        public string DriverVersion { get; set; }
        public int ImplicitTimeOut { get; set; }
        public int NumberOfAttempts { get; set; }
        public int PageLoadTimeout { get; set; }


        public void MapToStaticClass()
        {
            var sourceProperties = this.GetType().GetProperties();
            var destinationProperties = typeof(SettingsStatic)
                .GetProperties(BindingFlags.Public | BindingFlags.Static);

            foreach (var prop in sourceProperties)
            {
                var destinationProp = destinationProperties
                    .Single(p => p.Name == prop.Name);
                destinationProp.SetValue(null, prop.GetValue(this));
            }
        }

    }


}
