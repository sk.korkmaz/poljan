﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.WebDriver
{
    public class TimeOut : ITimeOut
    {
        public DateTime Now => DateTime.Now;

        public bool IsTimeOut(DateTime dateTime)
        {
            return DateTime.Now < dateTime;
        }

        public DateTime Later(TimeSpan time)
        {
            return DateTime.Now.Add(time);
        }
    }
}
