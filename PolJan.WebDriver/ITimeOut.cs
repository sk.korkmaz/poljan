﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.WebDriver
{
    public interface ITimeOut
    {
        DateTime Now { get; }
        DateTime Later(TimeSpan time);
        bool IsTimeOut(DateTime dateTime);
    }
}
