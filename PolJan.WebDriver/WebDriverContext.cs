﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using Microsoft.Edge.SeleniumTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolJan.WebDriver
{
    public sealed class WebDriverContext
    {
        private static WebDriverContext instance = null;
        private static readonly object padlock = new object();
        private static IWebDriver webDriver;
        private WebDriverContext(WebDriverApplication webDriverApplication)
        {
            IWebDriverOptions webDriverOptions = new WebDriverOptions();

            switch (webDriverApplication)
            {
                case WebDriverApplication.Chrome:
                    webDriver = new ChromeDriver(webDriverOptions.GetChromeDriverService(), webDriverOptions.GetChromeDriverOptions());
                    break;
                case WebDriverApplication.Edge:
                    webDriver = new EdgeDriver(webDriverOptions.GetEdgeDriverOptions());
                    break;
                default:
                    throw new ArgumentNullException("Application Type Is Not Found : null");
            }
            webDriver.Manage().Timeouts().PageLoad.Add(TimeSpan.FromSeconds(SettingsStatic.PageLoadTimeout));
        }

        private static WebDriverApplication _webDriverApplication;
        public static WebDriverApplication WebDriverApplication
        {
            get { return _webDriverApplication; }
            set 
            {
                _webDriverApplication = value;
            }
        }
        public static WebDriverContext Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new WebDriverContext(WebDriverApplication);
                    }
                    return instance;
                }

            }
        }

        public IWebDriver GetWebDriver
        {
            get
            {
                if (instance != null)
                {
                    return webDriver;
                }
                else
                {
                    return null;
                }
            }
        }
    }
}
